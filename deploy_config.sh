#!/bin/bash

CONFIG_TYPE=$1

mkdir -p -m 755 /home/sites/sky-fi
sudo chmod 755 -R /home/sites/sky-fi/
sudo chown w3user -R /home/sites/sky-fi/
mkdir -p -m 775 /logs/skyfi
sudo chmod 755 -R /logs/sky-fi/
sudo chown w3user -R /logs/skyfi/
sudo chgrp w3users -R /home/sites/sky-fi/

SCRIPT_DIR=$(dirname $(readlink -e $BASH_SOURCE))

for CONFIG_FILE in $(find $SCRIPT_DIR/config/$CONFIG_TYPE/ -type f -printf '/%P\n')
do
    CONFIG_SUBDIR=$(dirname $CONFIG_FILE)
    if [ ! -d $CONFIG_SUBDIR ]; then
        mkdir -p $CONFIG_SUBDIR
        chmod 755 $CONFIG_SUBDIR
    fi

    cp -rf $SCRIPT_DIR/config/$CONFIG_TYPE$CONFIG_FILE $CONFIG_FILE
    echo "$SCRIPT_DIR/config/$CONFIG_TYPE$CONFIG_FILE ==> $CONFIG_FILE"
done