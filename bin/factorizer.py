# -*- coding: utf-8 -*-

import cPickle

import pymongo as mongo
from rsvd import *
from utils.SimpleMapper import SimpleMapper
from utils.helpers import load_settings

def getRmse(model, probe, mean=8.):
    sqerr=0.0
    trivial_sqerr = 0.0
    for movieID,userID,rating in probe:
        err = rating - model(movieID,userID)
        sqerr += err * err

        trivial_err = rating - mean
        trivial_sqerr += trivial_err * trivial_err

    sqerr /= probe.shape[0]
    trivial_sqerr /= probe.shape[0]

    trivial_rmse = np.sqrt(trivial_sqerr)
    result_rmse = np.sqrt(sqerr)
    standart_rmse = 0.9514 * 2
    return (result_rmse, trivial_rmse, standart_rmse)


settings = load_settings('/etc/sky-fy.secret.json')
MONGO_CONN_STRING = "mongodb://%s:%s@%s:%s/books" % (settings['db']['user'],
                                               settings['db']['password'],
                                               settings['db']['host'],
                                               settings['db']['port'])

mng = mongo.MongoClient(MONGO_CONN_STRING)
db = mng.books

# Books feature matrix: [60681, 300]
# Trivial MSRE: 1.68020523694
# Model MSRE: 1.1335649334
# Accuracy increase: 32.534%

# Books feature matrix: [60681, 400]
# Trivial MSRE: 1.67972352824
# Model MSRE: 1.12615504554
# Accuracy increase: 32.956%


features_count = 300
books_threshold = 30
user_threshold = 1
test_size = 0.1

user_mapper = SimpleMapper()
book_mapper = SimpleMapper()
#b,u,r
ratings = []

books = {}
users = {}
print "Loading ratings, books threshold >= %d, users threshold >= %d" % (books_threshold, user_threshold)

print "Loading books dict"
bad_books = set([9910, 24999, 625357, 625358,
                 225615, 445811, 9909, 181078,
                 9911, 355259, 737353, 24906,
                 625359, 525111])
for x in db.ratings.find():
    bid = int(x['book_id'])
    if bid in bad_books:
        continue

    if bid not in books:
        books[bid] = [0, 0]
    books[bid][0] += 1
    books[bid][1] += int(x['rating'])

print "Loading users dict"
for x in db.ratings.find():
    bid = int(x['book_id'])

    if bid not in books or books[bid][0] < books_threshold:
        continue

    uid = int(x['user_id'])
    if uid not in users:
        users[uid] = 0
    users[uid] += 1

for bid in books:
    if books[bid][0] >= books_threshold:
        book_mapper.ident(bid)

for uid in users:
    if users[uid] >= user_threshold:
        user_mapper.ident(uid)

print "Creating dataset"
for x in db.ratings.find():
    uid = int(x['user_id'])
    bid = int(x['book_id'])
    rate = float(x['rating'])
    if book_mapper.getById(bid) and user_mapper.getById(uid):
        ratings.append((
            np.int32(book_mapper.getById(bid)),
            np.int32(user_mapper.getById(uid)),
            np.float(min(rate, 10.))
        ))


books_dict = {}
authors_dict = {}
print "Creating dicts"
for book_id in book_mapper.ids:
    book = db.books.find_one({'id': book_id})
    if not book:
        bad_books.add(book_id)
        print "Book id=%d not found" % book_id
        book = {'id': book_id, 'type': '?', 'title': 'Untitled', 'title_original': 'Untitled',
                'year': 1900, 'author_ids': [], 'author_title': 'No author', 'count': books_threshold }
        #raise Exception("Book id=%d not found" % book_id)

    author_title = []
    if book['author_ids']:
        authors = db.authors.find({ 'id': { '$in': book['author_ids']}})

        for a in authors:
            author_title.append(a['name'])
            authors_dict[a['id']] = a

    books_dict[book['id']] = {
        'id': book['id'],
        'type': book['type'],
        'title': book['title'],
        'title_original': book['title_original'],
        'year': book['year'],
        'author_ids': book['author_ids'],
        'author_title': u', '.join(author_title),
        'sum': books[book['id']][1],
        'count': book['count'],
   }

print "Bad books:", bad_books

np.random.shuffle(ratings)
rates = np.empty((len(ratings),), dtype=rating_t)
for i, r in enumerate(ratings):
    rates[i] = (r[0]+1, r[1], r[2])

n = int(rates.shape[0] * (1. - test_size))
train = rates[:n]
test = rates[n:]
print "[Users x Books] = [%d x %d]" % (user_mapper.len(), book_mapper.len())
print "Train / test sizes: %d / %d" % (train.shape[0], test.shape[0])
model = RSVD.train(features_count,
                   train,
                   (book_mapper.len(), user_mapper.len()),
                   probeArray=test,
                   learnRate=0.001,
                   regularization=0.07,
                   maxEpochs=500)

# learnRate=0.0005,
# regularization=0.03,
# Trivial MSRE: 1.67804060603
# Model MSRE: 1.11571984631
# Accuracy increase: 33.511%
# ----
# learnRate=0.001,
# regularization=0.05,
# Trivial MSRE: 1.68079249012
# Model MSRE: 1.11304738576
# Accuracy increase: 33.778%

result_rmse, trivial_rmse, standart_rmse = getRmse(model, test)
print "Books feature matrix: [%d, %d]" % model.u.shape
print "Trivial MSRE: " + str(trivial_rmse)
print "Model MSRE: " + str(result_rmse)
print "Accuracy increase: %.3f" % (100 - ( result_rmse * 100 / trivial_rmse)) + '%'

print "Saving data"
export_data = {
    'vector': model.u,
    'books': books_dict,
    'authors': authors_dict,
    'book_mapper': book_mapper.ids,
}
cPickle.dump(export_data, open('books_export.p', 'wb'))
    # np.int32(movie_mapper.getById(mid)),
    #         np.int32(user_mapper.ident(uid)),
    #         np.float(min(rate, 10.))
# np.random.shuffle(ratings)
# rates = np.empty((len(ratings),), dtype=rating_t)# (itemID,userID,rating)
# for i, r in enumerate(ratings):
#     rates[i] = (r[0]+1, r[1], r[2])
#
# #ratings = np.array(ratings, dtype=rating_t)
#
# n = int(rates.shape[0] * test_size)
# train = rates[:n]
# test = rates[n:]
# print "Train / test sizes: %d / %d" % (train.shape[0], test.shape[0])
#
# model = RSVD.train(features_count,
#                    train,
#                    (movie_mapper.len(), user_mapper.len()),
#                    probeArray=test,
#                    learnRate=0.005,
#                    regularization=0.005,
#                    maxEpochs=max_epochs)



