# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageColor, ImageFilter, ImageFont, ImageOps, ImageChops
import os
import pymongo as mongo
import numpy as np
import time
import logging
import argparse
from utils.helpers import load_settings

def load_data(limit=None):
    settings = load_settings('/etc/sky-fy.secret.json')
    MONGO_CONN_STRING = "mongodb://%s:%s@%s:%s/books" % (settings['db']['user'],
                                                   settings['db']['password'],
                                                   settings['db']['host'],
                                                   settings['db']['port'])

    mng = mongo.MongoClient(MONGO_CONN_STRING)
    db = mng.books
    x = []
    ids = {}
    scores = []
    book_dict = {}
    i = 0
    limit = limit if not limit is None else int(db.book_coords.count())
    for item in db.book_coords.find():
        # lon - x
        # lat = y
        x.append([float(item['lon']), float(item['lat'])])
        scores.append(float(item['sort']))
        ids[item['book_id']] = i
        i += 1
        # if limit and i > limit:
        #     break


    x = np.array(x, dtype=float)
    scores = np.array(scores, dtype=float)


    idx = (-scores).argsort()[:limit]
    j = 0
    ids2 = {}
    for i in idx:
        ids2[i] = j
        j += 1

    ids3 = {}
    for book_id in ids:
        if ids[book_id] in ids2:
            ids3[book_id] = ids2[ids[book_id]]

    x = x[idx, :]
    scores = scores[idx]

    for item in db.books.find({ 'id': { '$in': ids3.keys()}}):
        book_dict[ids3[item['id']]] = item

    return x, book_dict, scores
def check_fit(xy, radius, bounds):
    p = (int(xy[0]-radius), int(xy[1]-radius), int(xy[0]+radius), int(xy[1]+radius))
    for i in range(2):
        if p[i] < bounds[i]:
            bounds[i] = p[i]

    for i in range(2, 4):
        if p[i] > bounds[i]:
            bounds[i] = p[i]

def draw_circle(img, xy, radius, color, opacity=1., outline=False):
    f = Image.new('RGBA', (int(2*radius), int(2*radius)))
    d = ImageDraw.Draw(f)
    c = (int(color[0]), int(color[1]), int(color[2]), int(opacity*255))
    p = (0, 0, int(2*radius), int(2*radius))
    if outline:
        d.ellipse(p, fill=None, outline=c)
    else:
        d.ellipse(p, fill=c, outline=None)

    img.paste(f, mask=f, box=(int(xy[0]-radius), int(xy[1]-radius)))
    # f = Image.new('RGBA', img.size)
    # d = ImageDraw.Draw(f)
    # c = (color[0], color[1], color[2], int(opacity*255))
    # p =(int(xy[0]-radius), int(xy[1]-radius), int(xy[0]+radius), int(xy[1]+radius))
    # d.ellipse(p, fill=c, outline=None)
    # img.paste(f, mask=f)

def scale_to(value, src, trg=(0., 1.)):
    if trg[1] == trg[0]:
        return trg[1]
    value = (value - src[0]) / (src[1] - src[0])
    return trg[0] + value * (trg[1] - trg[0])

def get_color(point, mincolor=1.,  maxcolor=254., min_y=1960., max_y=2017., ctype=1):
    y = min(max(int(point['year']), min_y), max_y)
    year = int(maxcolor - scale_to(y, (min_y, max_y), (mincolor, maxcolor)))
    mc = int(maxcolor)
    y = int(year)
    y1 = min(int(year*1.8), int(maxcolor))
    y2 = min(int(year*2.5), int(maxcolor))
    if ctype == 1:
        if point['title_original'] != '':
            color = (int(maxcolor), min(int(year*1.8), int(maxcolor)), int(year))
        else:
            color = (int(year), min(int(year*2.5), int(maxcolor)), int(maxcolor))
    else:
        if point['title_original'] != '':
            color = (mc, y, y)
        else:
            color = (y, y, mc)


    return color



# ======================================================================================================================
# Parameters
#
# img = Image.new('RGBA', (500, 500), (11, 11, 11, 255))
# draw_circle(img, (150, 150), 100, (255, 0, 0), 0.7)
# draw_circle(img, (250, 250), 200, (0, 255, 0), 0.5)
# draw_circle(img, (250, 250), 77, (255, 255, 0), 0.9)
# filename = os.path.dirname(os.path.realpath(__file__)) + '/test6.jpg'
# img.save(filename, quality=90)
# exit()


parser = argparse.ArgumentParser()
parser.add_argument("--output-file", type=str, default="", help="")
parser.add_argument("--size", type=int, default=1000, help="")
parser.add_argument("--limit", type=int, default=50000, help="")
parser.add_argument("--min-star-size", type=int, default=1, help="")
parser.add_argument("--max-star-size", type=int, default=3, help="")

parser.add_argument("--max-zoom", type=int, default=17, help="")
parser.add_argument("--zoom-step", type=float, default=1.3, help="")
args = parser.parse_args()
args = vars(args)

#input_file = args['input_file']
filename = args['output_file']
axis_size = int(args['size'])
min_star_size = int(args['min_star_size'])
max_star_size = int(args['max_star_size'])


# background points count
limit = int(args['size'])

# target picture size in pixels (result size may vary)
size = (axis_size, axis_size)
#size = (5000, 5000)

bgcolor = (0, 0, 0)

jpeg_quality = 90

# global transparency of nebula [0..1]
global_alpha = .60

# projectile radius range for points - numbers in range[0..100]
project_radius = (0.1, 12.)#(0.1, 6.)

# projectile opacity range for points - numbers in range[0..1]
#project_opacity = (0.004, 0.005)
project_opacity = (0.006, 0.007)

# foreground stars limit
stars_limit = 60680

stars_opacity_range = (0.9, 1.0)

stars_color_range = (190, 255)

stars_radius_range = (min_star_size, max_star_size)
#stars_radius_range = (0.1, 1)

grid_count = 5

line_count = 16

grid_thickness = int(0.6 * (size[0]/1000))

grid_color = (180, 240, 255, 25)

# postprocess blur radius
blur_radius = 1 * (size[0]/500)

#filename = os.path.dirname(os.path.realpath(__file__)) + '/static/fullmap.jpg'
#filename = os.path.dirname(os.path.realpath(__file__)) + '/static/test.jpg'

#filename = os.path.dirname(os.path.realpath(__file__)) + '/fullmap.jpg'
#filename = os.path.dirname(os.path.realpath(__file__)) + '/static/test7.jpg'

# ======================================================================================================================
# 0. Initial data

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)

logging.info("0. Initializing settings")
logging.info("  Picture size: [%d, %d] pixels" % size)
x, points, scores = load_data(limit)

bounds = ((x[:, 0].min(), x[:, 1].min()), (x[:, 0].max(), x[:, 1].max()))
dims = ((bounds[1][0] - bounds[0][0]), (bounds[1][1] - bounds[0][1]))
center = (bounds[0][0] + dims[0] / 2., bounds[0][1] + dims[1] / 2.)

offset = (np.abs(bounds[0][0]), np.abs(bounds[0][1]))

scale_100 = (100 / dims[0],
             100 / dims[1])

scale_project = (size[0] / 100,
                 size[1] / 100)

scale_coords = (scale_100[0]*scale_project[0],
                scale_100[1]*scale_project[1])

inverse_scale = (dims[0] / size[0],
                 dims[1] / size[1])

common_scale = (scale_coords[0] + scale_coords[1]) / 2.
scale_scores = (scores.min(), scores.max())
offset = (np.abs(bounds[0][0]), np.abs(bounds[0][1]))

# ======================================================================================================================
# 1. get actual sizes
logging.info("1. Calculate target size")

project_bounds = [0., 0., 0., 0.]
for i in range(x.shape[0]):
    radius = scale_to(scores[i], scale_scores, project_radius)*scale_100[0] / 2.
    check_fit(((offset[0] + x[i, 0])*scale_coords[0], (offset[1] + x[i, 1])*scale_coords[1]), radius*scale_project[0], project_bounds)

for i in range(4):
    project_bounds[i] += int(np.sign(i-1.1) * blur_radius*3)

max_size = max((project_bounds[2] - project_bounds[0], project_bounds[3] - project_bounds[1]))
project_size = (max_size, max_size)
project_offset = (np.abs(project_bounds[0]), np.abs(project_bounds[1]))
logging.info("  Corrected size: [%d, %d] pixels" % project_size)
logging.info("  Bounds in pixels: [%d, %d, %d, %d]" % tuple(project_bounds))
logging.info("  Offset in pixels: [%d, %d]" % project_offset)
logging.info("  Scaling coefficients: [%.8f, %.8f]" % inverse_scale)
logging.info("  Bounds offset: [ [%f, %f], [%f, %f] ]" % (project_bounds[0]*inverse_scale[0] - offset[0] - bounds[0][0],
                                           project_bounds[1]*inverse_scale[1] - offset[1] - bounds[0][1],
                                           project_bounds[2]*inverse_scale[0] - offset[0] - bounds[1][0],
                                           project_bounds[3]*inverse_scale[1] - offset[1] - bounds[1][1]))

# ======================================================================================================================
# 2. draw background
logging.info("2. Rendering background (nebula and grid)")

logged = time.time()
log_period = 5
background = Image.new('RGBA', project_size, (bgcolor[0], bgcolor[1], bgcolor[2], 255))

# ------------------------------------------
logging.info(" - rendering grid circles")
grid = Image.new('RGBA', project_size)
grid_step = max(dims)/2 / grid_count
for i in range(grid_count):
    radius = int(grid_step*(grid_count-i)*max(scale_coords)*0.95)
    xy = (project_offset[0]+(offset[0] + center[0])*scale_coords[0],
          project_offset[1]+(offset[1] + center[1])*scale_coords[1])
    f = Image.new('RGBA', (int(2*radius), int(2*radius)))
    d = ImageDraw.Draw(f)
    c = grid_color

    p = (0, 0, int(2*radius), int(2*radius))
    d.ellipse(p, fill=c, outline=None)

    p = (grid_thickness, grid_thickness, int(2*radius)-grid_thickness, int(2*radius)-grid_thickness)
    d.ellipse(p, fill=bgcolor, outline=None)
    grid.paste(f, mask=f, box=(int(xy[0]-radius), int(xy[1]-radius)))

# ------------------------------------------
logging.info(" - rendering grid lines")
line_step = 2*np.pi / line_count
radius = int(grid_step*(grid_count)*max(scale_coords))
xy = (project_offset[0]+(offset[0] + center[0])*scale_coords[0],
      project_offset[1]+(offset[1] + center[1])*scale_coords[1])
f = Image.new('RGBA', (int(2*radius), int(2*radius)))
d = ImageDraw.Draw(f)
for i in range(line_count):
    rx = np.cos(i*line_step)
    ry = np.sin(i*line_step)
    #print ((xy[0], (xy[1]), (xy[0]+radius)*rx, (xy[1]+radius)*ry)
    d.line((radius, radius, radius+(radius)*rx, radius+(radius)*ry), width=grid_thickness, fill=grid_color)
grid.paste(f, mask=f, box=(int(xy[0]-radius), int(xy[1]-radius)))

# ------------------------------------------
logging.info(" - rendering color layer #1")
figure2 = Image.new('RGBA', project_size)
for i in range(x.shape[0]):
    radius = scale_to(scores[i], scale_scores, project_radius)*scale_100[0] / 1.3
    opacity = scale_to(scores[i], scale_scores, project_opacity)
    if i in points:
        color = get_color(points[i], min_y=1850, ctype=2)
    else:
        logging.error('point %d not exists' % i)
        continue
        #color = (100, 100, 100)
    draw_circle(figure2,
                (project_offset[0]+(offset[0] + x[i, 0])*scale_coords[0],
                 project_offset[1]+(offset[1] + x[i, 1])*scale_coords[1]),
                int(radius*scale_project[0]),
                color,
                opacity)
    if (time.time() - logged) > log_period:
        logging.info("      Processed %.1f%%" % (100. * i / x.shape[0]))
        logged = time.time()

# ------------------------------------------
logging.info(" - rendering color layer #2")
figure = Image.new('RGBA', project_size)
for i in range(x.shape[0]):
    radius = scale_to(scores[i], scale_scores, project_radius)*scale_100[0] / 2.
    opacity = scale_to(scores[i], scale_scores, project_opacity)
    if i in points:
        color = get_color(points[i], min_y=1850,)
    else:
        logging.error('point %d not exists' % i)
        continue
        #color = (100, 100, 100)
    draw_circle(figure,
                (project_offset[0]+(offset[0] + x[i, 0])*scale_coords[0],
                 project_offset[1]+(offset[1] + x[i, 1])*scale_coords[1]),
                int(radius*scale_project[0]),
                color,
                opacity)
    if (time.time() - logged) > log_period:
        logging.info("      Processed %.1f%%" % (100. * i / x.shape[0]))
        logged = time.time()

logging.info(" - blending")
figure = Image.blend(figure2, figure, 0.7)
#img = Image.blend(background, figure, alpha=1.)
#img = Image.alpha_composite(background, figure)
# ======================================================================================================================
# 3. post processing background
logging.info("3. Post-processing background")

alpha = Image.new('L', background.size, int(global_alpha*255))
figure.putalpha(alpha)

background.paste(figure, mask=figure)
background = background.filter(ImageFilter.GaussianBlur(blur_radius))
background = ImageChops.lighter(grid, background)
#background.paste(grid, mask=figure)

# ======================================================================================================================
# 4. draw foreground stars
logging.info("4. Rendering foreground (stars-field)")
figure = Image.new('RGBA', project_size)
x, points, scores = load_data(stars_limit)
for i in range(x.shape[0]):
    #opacity = 0.6
    opacity = scale_to(scores[i], scale_scores, stars_opacity_range)
    c = int(scale_to(scores[i], scale_scores, stars_color_range))
    color = (c*0.6, c*0.8, c)
    radius = int(scale_to(scores[i], scale_scores, stars_radius_range))
    draw_circle(figure,
                (project_offset[0]+(offset[0] + x[i, 0])*scale_coords[0],
                 project_offset[1]+(offset[1] + x[i, 1])*scale_coords[1]),
                radius,
                color,
                opacity)
    if (time.time() - logged) > log_period:
        logging.info("      Processed %.1f%%" % (100. * i / x.shape[0]))
        logged = time.time()

background.paste(figure, mask=figure)
# ======================================================================================================================
# 4. saving
logging.info("5. Saving")
img = ImageOps.flip(background)

img.save(filename, quality=jpeg_quality)
logging.info("  Saved to %s" % filename)
logging.info("  Bounds in pixels: [%d, %d, %d, %d]" % tuple(project_bounds))
logging.info("  Offset in pixels: [%d, %d]" % project_offset)
logging.info("  Scaling coefficients: [%.8f, %.8f]" % inverse_scale)
logging.info("  Bounds offset: [ [%f, %f], [%f, %f] ]" % (project_bounds[0]*inverse_scale[0] - offset[0] - bounds[0][0],
                                           project_bounds[1]*inverse_scale[1] - offset[1] - bounds[0][1],
                                           project_bounds[2]*inverse_scale[0] - offset[0] - bounds[1][0],
                                           project_bounds[3]*inverse_scale[1] - offset[1] - bounds[1][1]))
