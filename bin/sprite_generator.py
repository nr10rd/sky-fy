from PIL import Image, ImageDraw, ImageFilter
import numpy as np

def draw_ellipse(img, radius, color):
    f = Image.new('RGBA', img.size)
    d = ImageDraw.Draw(f)
    p = (radius, radius, img.size[0]-radius, img.size[1]-radius)
    d.ellipse(p, fill=color, outline=None)
    img.paste(f, mask=f)

def draw_line(img, angle, radius, color, width=1):
    center = (img.size[0]/2., img.size[1]/2.)

    f = Image.new('RGBA', img.size)
    d = ImageDraw.Draw(f)

    rx = np.cos(angle)
    ry = np.sin(angle)
    #print ((xy[0], (xy[1]), (xy[0]+radius)*rx, (xy[1]+radius)*ry)
    d.line((center[0] - (radius)*rx,
            center[1] - (radius)*ry,
            center[0] + (radius)*rx,
            center[1] + (radius)*ry),
           width=width, fill=color)
    img.paste(f, mask=f)

def scale_to(v, src, trg):
    if trg[1] == trg[0]:
        return trg[0]
    return int(trg[0] + ((v - src[0]) / float(src[1] - src[0])) * (trg[1] - trg[0]))

def draw_star(img, rrange, orange, powval=4., blur=3):

    for radius in range(rrange[0], rrange[1], 5):
        rr = (radius)**powval
        scaled = orange[0] + ((rr - rrange[0]**powval) / float(rrange[1]**powval - rrange[0]**powval)) * (orange[1] - orange[0])
        o = int(scaled)
        draw_ellipse(img, radius, (255, 255, 255, o))

    img = img.filter(ImageFilter.GaussianBlur(3))
    return img

def draw_ray(img, angle, rrange, orange, width=1, powval=5):
    for radius in range(rrange[0], rrange[1], 10):
        rr = (rrange[1] - radius)**powval
        scaled = orange[0] + ((rr - rrange[0]**powval) / float(rrange[1]**powval - rrange[0]**powval)) * (orange[1] - orange[0])
        draw_line(img, angle, radius, (255, 255, 255, int(scaled)))

def tone_color(img, color, orange):
    size = img.size
    pixels = img.load()
    for y in xrange(size[1]):
        for x in xrange(size[0]):
            o = min([pixels[x, y][0], pixels[x, y][1], pixels[x, y][2]])
            c = max([pixels[x, y][0],pixels[x, y][1],pixels[x, y][2]])
            #o = pixels[x, y][0]
            #pixels[x, y] = (pixels[x, y][0], pixels[x, y][1], pixels[x, y][2], o)
            o = orange[0] + ((o) / float(255)) * (orange[1] - orange[0])
            pixels[x, y] = (scale_to(o, orange, (color[0], 255)),
                            scale_to(o, orange, (color[1], 255)),
                            scale_to(o, orange, (color[2], 255)), int(o))

def draw_template(img, template_number, powval=9):
    size = img.size
    if template_number == 0:
        #1
        img = draw_star(img, (int(size[0]*0.025), int(size[0]*0.45)), (2, 255), powval=powval, blur=3)
        draw_ray(img, np.pi, (int(size[0]*0.025), int(size[0]*0.4)), [20, 80], powval=5)
        draw_ray(img, np.pi*1.3, (int(size[0]*0.025), int(size[0]*0.3)), [10, 20], powval=5)
        draw_ray(img, np.pi*1.35, (int(size[0]*0.025), int(size[0]*0.3)), [10, 20], powval=5)
        draw_ray(img, np.pi*0.5, (int(size[0]*0.025), int(size[0]*0.4)), [20, 80], powval=5)
    elif template_number == 1:
        #2
        img = draw_star(img, (int(size[0]*0.025), int(size[0]*0.45)), (2, 255), powval=powval, blur=3)
        draw_ray(img, np.pi, (int(size[0]*0.025), int(size[0]*0.35)), [10, 150], powval=2)
    elif template_number == 2:
        #3
        img = draw_star(img, (int(size[0]*0.025), int(size[0]*0.45)), (2, 255), powval=powval, blur=3)
        draw_ray(img, np.pi*0.5, (int(size[0]*0.025), int(size[0]*0.30)), [20, 60], powval=5)
        draw_ray(img, np.pi*1.6, (int(size[0]*0.025), int(size[0]*0.37)), [10, 40], powval=5)
        draw_ray(img, np.pi*1.65, (int(size[0]*0.025), int(size[0]*0.26)), [10, 20], powval=5)
        draw_ray(img, np.pi*1.3, (int(size[0]*0.025), int(size[0]*0.26)), [10, 20], powval=5)
        draw_ray(img, np.pi*1.35, (int(size[0]*0.025), int(size[0]*0.37)), [10, 40], powval=5)
        draw_ray(img, np.pi, (int(size[0]*0.025), int(size[0]*0.4)), [20, 60], powval=5)
    else:
        #4
        img = draw_star(img, (int(size[0]*0.025), int(size[0]*0.45)), (2, 255), powval=powval, blur=3)
        draw_ray(img, np.pi*0.5, (int(size[0]*0.025), int(size[0]*0.30)), [10, 30], powval=5)
        draw_ray(img, np.pi*1.25, (int(size[0]*0.025), int(size[0]*0.37)), [20, 80], powval=5)
        draw_ray(img, np.pi*1.75, (int(size[0]*0.025), int(size[0]*0.26)), [10, 80], powval=5)
        draw_ray(img, np.pi, (int(size[0]*0.025), int(size[0]*0.3)), [10, 30], powval=5)
    return img

bin_colors = (145, 255)
sizes = [(64, 64),
         (56, 56),
         (48, 48),
         (40, 40),
         (32, 32),
         (24, 24),
         (16, 16)]

colors = [(1, 1, 1),
          (0, 1, 1),
          (1, 0, 1),
          (1, 1, 0),
          (0, 0, 1),
          (1, 0, 0),
          (0, 1, 0)]

file_template = "static/star_%d_%d%d%d.png"

# background = draw_template(background, 1)
# tone_color(background, color, (4, 255))
# filename = 'test.png'
# background.save(filename, 'PNG')
i = 0
for color in colors:
    template_id = i
    img = Image.new('RGBA', (200, 200))
    img = draw_template(img, template_id)
    tone_color(img, (bin_colors[color[0]], bin_colors[color[1]], bin_colors[color[2]]), (4, 255))
    for size in sizes:
        img_resized = img.resize(size, Image.LANCZOS)
        filename = file_template % (size[0], color[0], color[1], color[2])
        img_resized.save(filename, 'PNG')
    i += 1