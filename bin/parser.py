# -*- coding: utf-8 -*-

# Алгоритм сбора инфы с фантлаба
# 1) Идем по списку юзеров (простым инкрементным перебором) http://fantlab.ru/user[%d] в диапазоне [1..154851]
#     - оценки лежат: http://fantlab.ru/user[%d]/marks (можно итерироваться сразу по этим страницам)
#        - идем по странице: тянем ссылки на книжки + оценки.
#        - переходим по страницам
#        - формат записей:  Станислав Лем «Звёздные дневники Ийона Тихого» / «Dzienniki gwiazdowe Ijona Tichego» [Цикл]
#                               автор ссылка-id-название [/ название в переводе] [тип - цикл, сборник и тп]
#
# - после первого шага - уже можно делать UV-разложение и строить tsne по книгам
# - можно делать тоже самое по авторам (но без детальной инфы про авторов)
#
# 2) Идем по полученному списку книжек
#     - тянем ссылку-id-имя автора
#     - год публикации
#     - тянем жанры (id, название, название группы)
# 3) Идем по списку авторов
#      - тянем
import random
import time

import requests
import bs4
import pymongo as mongo

from project.lib.fantlab.entity import *
from fantlab.utils import Cacher


MONGO_CONN_STRING = "mongodb://127.0.0.1:27017"
mng = mongo.MongoClient(MONGO_CONN_STRING)
db = mng.books
cache = Cacher('fantlab_cache', '/home/akozlovtsev/tmp/cache/')


def create_indexes(db):
    db.books.create_index('id', unique=True)
    db.books.create_index('count')
    db.books.create_index('sum')
    db.books.create_index('author_ids')
    db.authors.create_index('id', unique=True)
    db.users.create_index('id', unique=True)
    db.ratings.create_index('user_id')
    db.ratings.create_index([("user_id", mongo.ASCENDING), ("book_id", mongo.ASCENDING)], unique=True)
    db.genres.create_index('id', unique=True)

# 1. User rates page:
#   - user (id + name)
#   - [books (id + name + type), ...]
#   - (user_id, book_id, rating)
#   - next-page-url / None
def process_user_marks_page(url_tpl, user_id, page=1, data=None, pause_range=[0.05, 3]):
    users, books, rates, authors = data
    next_page_url = ''
    url = url_tpl % (user_id, page)

    user = UserEntity({
        'id': user_id
    })

    if not cache.is_cached(url):
        pause = pause_range[0] + random.random() * (pause_range[1]-pause_range[0])
        time.sleep(pause)
        # print "Cached entities are over"
        # exit()

    request = cache.cached(url, lambda: requests.get(url))
    if request.status_code != 200:
        return (users, books, rates, authors), next_page_url

    parser = bs4.BeautifulSoup(request.text, 'lxml')

    #user-login
    rows = parser.find_all('div', class_='center')
    if rows:
        for r in rows:
            user.name = (' '.join(r.text.split(' ')[1:]).strip(' \n\t\r')).encode('utf-8')
    users[user_id] = user

    #next page
    main = parser.find('main', class_='content')
    rows = main.find_all('div', limit=3)
    if len(rows)>2 and rows[2].text:
        is_pager = False
        for p in rows[2].text.split(' ')[1:]:
            if p == '[%d]' % page:
                is_pager = True
            elif is_pager:
                next_page_url = url_tpl % (user_id, int(p))
                break

    #books
    table = main.find('table', class_='v9b')
    for tr in table.find_all('tr')[1:]:
        tds = tr.find_all('td')[0:2]
        titlestr = tds[0]
        origin_title = ''
        book_type = u'Роман'
        if tds[1].text == '':
            continue

        book_authors = []
        authorstr = u'.'.join(titlestr.text.split(u'«')[0].split(u'.')[1:])
        for authorsubstr in authorstr.split(u', '):
            nm = authorsubstr.encode('utf-8').strip('   /n/t/r')
            if nm not in authors:
                author = AuthorEntity({
                    'id': len(authors)+1,
                    'name': nm
                })
                authors[nm] = author
            author = authors[nm]
            book_authors.append(author)

        rating = int(tds[1].text)
        title = titlestr.find('a').text
        book_id = int(titlestr.find('a').attrs['href'].replace('/work', ''))
        if book_id not in books:
            if len(titlestr.text.split('/')) > 1:
                subtitle = '/'.join(titlestr.text.split('/')[1:])
                if len(subtitle.split('[')) > 1:
                    book_type = '['.join(subtitle.split('[')[1:]).strip('[]')
                    subtitle = subtitle.split('[')[0].strip(' ')
                origin_title = subtitle
            book = BookEntity({
                'id': book_id,
                'count': 0,
                'sum': 0,
                'title': title.encode('utf-8'),
                'origin_title': origin_title.encode('utf-8'),
                'form': book_type.encode('utf-8'),
            })
            for author in book_authors:
                book.add_author(author)

            # if book_id == 264:
            #     print book.author_ids
            #     for author in book_authors:
            #         print author.id, author.name
            books[book_id] = book

        books[book_id].count += 1
        books[book_id].sum += rating
        rate_item = (user_id, book_id, rating)
        rates.append(rate_item)

        #print '#%d - %s (%s) %s - %d' % (book_id, title, subtitle, book_type, rating)
        #print book.to_dict()
    # next-page:
    return (users, books, rates, authors), next_page_url

def save_data(db, data):
    (users, books, rates, authors) = data
    #print "[Users x Books] = [%d x %d]" % (len(users), len(books))
    #print "Ratings: %d" % len(rates)
    for col, data in [('users', users), ('books', books)]:
        for id in data:
            try:
                getattr(db, col).insert(data[id].to_dict(), continue_on_error=True)
            except Exception as e:
                pass

    for tmp in authors:
        try:
            db.authors.insert(authors[tmp].to_dict(), continue_on_error=True)
        except Exception as e:
            pass

    for (u, b, r) in rates:
        try:
            db.ratings.insert({
                'user_id': u,
                'book_id': b,
                'rating': r
            }, continue_on_error=True)
        except Exception as e:
            pass

def iterate_user_marks_pages(url_tpl, start_user=1, end_user=154851, limit=0):
    maxiter = end_user - start_user
    users = {}
    books = {}
    authors = {}
    rates = []
    data = (users, books, rates, authors)
    logged = time.time()
    log_period = 20
    lr = 0

    saved_books = set([])
    saved_users = set([])
    saved_authors = set([])
    saved_rates = 0

    for author in db.authors.find():
        nm = author['name'].encode('utf-8')
        authors[nm] = AuthorEntity({
            'id': author['id'],
            'name': nm
        })
    for u in range(maxiter):
        try:
            page = 1
            next_page_url = 'Ok'
            while next_page_url:
                data, next_page_url = process_user_marks_page(url_tpl, start_user+u, page, data=data)
                page += 1

            if limit > 0 and len(data[0]) > limit:
                break

            if (time.time() - logged) > log_period:
                print "Processed %d users (%d saved), collected %d books (%d saved), " \
                      "collected %d authors (%d saved), " \
                      "%d ratings (%d saved), last_user_id: %d, speed: %.2f rates/s" % (len(data[0])+len(saved_users),
                                                                                        len(saved_users),
                                                                                        len(data[1])+len(saved_books),
                                                                                        len(saved_books),
                                                                                        len(data[3])+len(saved_authors),
                                                                                        len(saved_authors),
                                                                                        len(data[2])+saved_rates,
                                                                                        saved_rates,
                                                                                        start_user+u,
                                                                                        (len(data[2])-lr)/(time.time()-logged))
                lr = len(data[2])
                logged = time.time()

            if len(data[2]) > 50000:
                print "Saving tmp data"
                us = {}
                bs = {}
                aus = {}
                for u in data[0]:
                    if u not in saved_users:
                        us[u] = data[0][u]
                        saved_users.add(u)
                for b in data[1]:
                    if b not in saved_books:
                        bs[b] = data[1][b]
                        saved_books.add(b)
                for a in data[3]:
                    if a not in saved_authors:
                        aus[a] = data[3][a]
                        saved_authors.add(a)



                saved_rates += len(data[2])
                save_data(db, (us, bs, data[2], aus))

                for id in data[1]:
                    db.books.update({'id': id}, {
                        '$inc': {
                            'sum': data[1][id].sum,
                            'count': data[1][id].count,
                        }
                    })

                data = ({}, {}, [], data[3])
                lr = 0

        except Exception as e:
            print 'Exception', e.message

    return data

#user, books, rates, next_page_url = process_user_marks_page('http://fantlab.ru/user%d/markspage%d', 1, 1)
#user, books, rates, next_page_url = process_user_marks_page('http://fantlab.ru/user%d/markspage%d', 306, 1)
#user, books, rates, next_page_url = process_user_marks_page('http://fantlab.ru/user%d/markspage%d', 303, 1)

print "Check indexes"
create_indexes(db)

last_user_id = int(db.users.find().sort([('id', mongo.DESCENDING)]).limit(1)[0]['id'])
print "Parse data from id=%d" % last_user_id
data = iterate_user_marks_pages('http://fantlab.ru/user%d/markspage%d', start_user=last_user_id)
print "Save data"
save_data(db, data)
