# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageColor, ImageFilter, ImageFont, ImageOps, ImageChops
import os
import pymongo as mongo
import numpy as np
import time
import logging
import argparse
import glob
from operator import itemgetter

def get_scale(zoom, zoom_step):
    return zoom_step**zoom

def slice_image(img, size, tile_size, dir=''):
    total_size = 0
    N = int(np.ceil(size / tile_size))
    last_logged = time.time()
    i = 0
    log_period = 1
    for x in range(N):
        for y in range(N):
            slice_box = (x*tile_size, y*tile_size, min((x+1)*tile_size, size),
                                                  min((y+1)*tile_size, size))
            slice = img.crop(slice_box)
            if (slice_box[2] - slice_box[0]) < tile_size or (slice_box[3] - slice_box[1]) < tile_size:
                back = Image.new('RGBA', (tile_size, tile_size))
                back.paste(slice, box=(0, 0, slice.size[0], slice.size[1]))
                slice = back

            xpath = "%s/%d/" % (dir, x)
            if not os.path.exists(xpath):
                os.makedirs(xpath)
            ypath = "%s/%d/%d.png" % (dir, x, y)
            slice.save(ypath)
            i += 1
            total_size += os.stat(ypath).st_size
            if (time.time() - last_logged) > log_period:
                logging.info("      processed %.2f%%" % (100.*i/(N**2)))
                last_logged = time.time()
    return total_size

def find_best_img(size, imgs):
    v = size[0] * size[1]
    m = None
    f = None
    for i in imgs:
        diff = abs(v - i[1])
        if m is None or diff < m:
            m = diff
            f = i

    return f[0]

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)

parser = argparse.ArgumentParser()
parser.add_argument("--input-file", type=str, default="", help="")
parser.add_argument("--output-dir", type=str, default="", help="")
parser.add_argument("--min-zoom", type=int, default=5, help="")
parser.add_argument("--max-zoom", type=int, default=18, help="")
parser.add_argument("--zoom-step", type=float, default=1.3, help="")
args = parser.parse_args()
args = vars(args)

input_file = args['input_file']
output_dir = args['output_dir']

zoom_step = float(args['zoom_step'])
zoom_range = (int(args['min_zoom']), int(args['max_zoom']))



if not input_file:
    logging.error('Input file not exists')
    parser.print_help()
    exit()

if not output_dir or not os.path.exists(output_dir):
    logging.error('Output dir not exists')
    parser.print_help()
    exit()

src_images = []
input_files = []
for f in glob.glob(input_file):
    print f
    img = Image.open(f)
    w, h = img.size
    src_images.append((f, w*h, w, h))
src_images.sort(key=itemgetter(1), reverse=True)

if len(src_images) == 0:
    logging.error('Input file not exists')
    parser.print_help()
    exit()

input_file = src_images[0][0]

output_dir = output_dir.rstrip(' /')

logging.info("Loading image")
img = Image.open(input_file)
#os.makedirs()

image_size = img.size
if image_size[0] != image_size[1]:
    logging.error('Only quadratic images allowed!')
    exit()

tile_size = 256

#zoom_range = (5, 6)
tiles_count = 0

logging.info("Info:")
logging.info("Source image size: %dx%d" % image_size)
logging.info("Zoom step: %.1f" % zoom_step)
logging.info("Zoom range: [%d..%d]" % zoom_range)

logging.info("Start generation")
total_size = 0
for zoom in range(zoom_range[0], zoom_range[1]+1):
    scaled_size = get_scale(zoom, zoom_step)*tile_size
    tiles = np.ceil(scaled_size / tile_size)
    tiles_count += tiles**2
    #print zoom, scaled_size, tiles, tiles*tile_size

    logging.info("Processing zoom: %d" % zoom)

    best_img_filename = find_best_img((scaled_size, scaled_size), src_images)
    logging.info("  Resizing image to %dx%d" % (scaled_size, scaled_size))
    logging.info("  Using source image: %s" % best_img_filename)
    img = Image.open(best_img_filename)


    img_resized = img.resize((int(np.round(scaled_size)), int(np.round(scaled_size))), Image.LANCZOS)

    logging.info("  Slicing image to %dx%d tiles" % (tiles, tiles))
    total_size += slice_image(img_resized, scaled_size, tile_size, "%s/%d" % (output_dir, zoom))


logging.info("Done. Total files: %d, total size: %.2fMB" %(tiles_count, total_size / 1024./ 1024.))
