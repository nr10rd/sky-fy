from datetime import datetime

class BaseEntity(object):
    def __init__(self, data={}):
        self.from_dict(data)

    def to_dict(self):
        return {}

    def from_dict(self, data={}):
        for k in data:
            setattr(self, k, data[k])

class CountryEntity(BaseEntity):
    id_ = None
    name_ = ''
    
    @property
    def id(self):
        return self.id_
    
    @id.setter
    def id(self, value):
        self.id_ = int(value)
        
    @property
    def name(self):
        return self.name_
    
    @name.setter
    def name(self, value):
        self.name_ = str(value)

    def from_fantlab_dict(self, data):
        map = {
            'country_id': 'id',
            'country_name': 'name',
        }
        for k in data:
            if k in map and not data[k] is None:
                if type(data[k]) not in {dict, list}:
                    data[k] = data[k].encode('utf-8')
                setattr(self, map[k], data[k])

    def to_dict(self):
        return dict(
            id=self.id,
            name=self.name,

        )

class GenreEntity(BaseEntity):
    id_ = None
    name_ = ''
    group_ = ''

    @property
    def id(self):
        return self.id_

    @id.setter
    def id(self, value):
        self.id_ = str(value)

    @property
    def name(self):
        return self.name_

    @name.setter
    def name(self, value):
        self.name_ = str(value)

    @property
    def group(self):
        return self.group_

    @group.setter
    def group(self, value):
        self.group_ = str(value)

    def to_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            group=self.group
        )

class UserEntity(BaseEntity):
    id_ = None
    name_ = ''

    @property
    def id(self):
        return self.id_

    @id.setter
    def id(self, value):
        self.id_ = int(value)

    @property
    def name(self):
        return self.name_

    @name.setter
    def name(self, value):
        self.name_ = str(value)


    def to_dict(self):
        return dict(
            id=self.id,
            name=self.name,

        )

# ----------------------------------------------------------------------------------------------------------------------

class AuthorEntity(BaseEntity):
    id_ = None
    name_ = ''
    name_original_ = ''
    gender_male_ = True
    image_id_ = ''
    country_id_ = 0
    birthday_ = None
    deathday_ = None
    
    @property
    def id(self):
        return self.id_
    
    @id.setter
    def id(self, value):
        self.id_ = int(value)
        
    @property
    def name(self):
        return self.name_
    
    @name.setter
    def name(self, value):
        self.name_ = str(value)

    @property
    def name_original(self):
        return self.name_original_

    @name_original.setter
    def name_original(self, value):
        self.name_original_ = str(value)

    @property
    def gender_male(self):
        return self.gender_male_

    @gender_male.setter
    def gender_male(self, value):
        self.gender_male_ = bool(value)

    @property
    def birthday(self):
        return self.birthday_.isoformat().split('T')[0]

    @birthday.setter
    def birthday(self, value):
        self.birthday_ = datetime.strptime(str(value), '%Y-%m-%d')


    @property
    def deathday(self):
        return self.deathday_.isoformat().split('T')[0]

    @deathday.setter
    def deathday(self, value):
        self.deathday_ = datetime.strptime(str(value), '%Y-%m-%d')

    @property
    def image_id(self):
        return self.image_id_

    @image_id.setter
    def image_id(self, value):
        self.image_id_ = int(value)


    @property
    def country_id(self):
        return self.country_id_

    @country_id.setter
    def country_id(self, value):
        """
        :param CountryEntity value:
        :return:
        """
        self.country_id_ = value

    def from_fantlab_dict(self, data):
        map = {
            'autor_id': 'id',
            'gender_male': 'gender_male',
            'image_id': 'image_id',
            'birthday': 'birthday',
            'deathday': 'deathday',
            'name': 'name',
            'name_orig': 'name_original',
            'country_id': 'country_id'
        }
        if data['birthday']:
            data['birthday'] = data['birthday'].replace('-00', '-01').replace('0000', '1900')
        if data['deathday']:
            data['deathday'] = data['deathday'].replace('-00', '-01').replace('0000', '1901')

        data['gender_male'] = data['sex'] == 'm'
        data['image_id'] = data['image'].split('/')[-1] if data['image'] else 0

        for k in data:
            if k in map and not data[k] is None:
                if type(data[k]) not in {dict, list, bool, int}:
                    data[k] = data[k].encode('utf-8')
                setattr(self, map[k], data[k])

    def to_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            name_original=self.name_original,
            gender_male=self.gender_male,
            image_id=self.image_id,
            country_id=self.country_id,
            birthday=self.birthday if self.birthday_ else None,
            deathday=self.deathday if self.deathday_ else None,
        )

# ----------------------------------------------------------------------------------------------------------------------

class BookEntity(BaseEntity):
    id_ = None
    title_ = ''
    title_original_ = ''
    year_ = 0
    year_of_write_ = 0
    author_ids_ = set([])
    annotation_ = ''
    image_id_ = 0
    type_ = ''
    rating_ = 0.
    count_ = 0

    def __init__(self, data={}):
        super(BookEntity, self).__init__(data)
        self.authors_ = {}
        self.genres_ = {}

    @property
    def id(self):
        return self.id_

    @id.setter
    def id(self, id_):
        self.id_ = int(id_)

    @property
    def title(self):
        return self.title_

    @title.setter
    def title(self, value):
        self.title_ = str(value)

    @property
    def title_original(self):
        return self.title_original_

    @title_original.setter
    def title_original(self, value):
        self.title_original_ = str(value)

    @property
    def year(self):
        return self.year_

    @year.setter
    def year(self, value):
        self.year_ = int(value)

    @property
    def year_of_write(self):
        return self.year_of_write_

    @year_of_write.setter
    def year_of_write(self, value):
        self.year_of_write_ = int(value)

    @property
    def rating(self):
        return self.rating_

    @rating.setter
    def rating(self, value):
        self.rating_ = float(value)

    @property
    def image_id(self):
        return self.image_id_

    @image_id.setter
    def image_id(self, value):
        self.image_id_ = int(value)


    @property
    def type(self):
        return self.type_

    @type.setter
    def type(self, value):
        self.type_ = str(value)

    @property
    def author_ids(self):
        return self.author_ids_

    @author_ids.setter
    def author_ids(self, value):
        for v in value:
            self.add_author_id(v)

    def add_author_id(self, author_id):
        """
        :param AuthorEntity author: AuthorEntity
        :return:
        """
        self.author_ids_.add(int(author_id))

    @property
    def annotation(self):
        return self.annotation_

    @annotation.setter
    def annotation(self, value):
        self.annotation_ = str(value)


    @property
    def count(self):
        return self.count_

    @count.setter
    def count(self, value):
        self.count_ = int(value)

    def from_fantlab_dict(self, data):
        map = {
            'work_id': 'id',
            'work_name': 'title',
            'work_name_orig': 'title_original',
            'work_type': 'type',
            'work_year': 'year',
            'work_year_of_write': 'year_of_write',
            'rating': 'rating',
            'count': 'count',
            'author_ids': 'author_ids'
        }
        data['image_id'] = data['image'].split('/')[-1] if data['image'] else 0
        data['count'] = data['rating']['voters'] if type(data['rating']['voters']) == int or data['rating']['voters'].isdigit() else 0
        data['rating'] = data['rating']['rating'] if type(data['rating']['rating']) == int or data['rating']['rating'].isdigit() else 0
        self.author_ids_ = set([])
        data['author_ids'] = []
        for a in data['authors']:
            if a['type'] == 'autor':
                data['author_ids'].append(a['id'])


        for k in data:
            if k in map and not data[k] is None:
                if not type(data[k]) in {dict, list, int}:
                    data[k] = data[k].encode('utf-8')
                setattr(self, map[k], data[k])

    def to_dict(self):
        return dict(
            id=self.id,
            title=self.title,
            title_original=self.title_original,
            type=self.type,
            year=self.year,
            year_of_write=self.year_of_write,
            annotation=self.annotation,
            author_ids=list(self.author_ids_),
            rating=self.rating,
            count=self.count
        )

# ----------------------------------------------------------------------------------------------------------------------

