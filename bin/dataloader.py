# -*- coding: utf-8 -*-

# Загрузчик "родных" данных фантлаба
# Умеет:
# - импортировать оценки
# - парсить данные о книгах/авторах/жанрах/тегах

import random
import time
import os
import json
import argparse
import logging
import csv

import requests
import pymongo as mongo

from skyfi.entity import *
from utils.Cacher import Cacher
from utils.helpers import load_settings


def create_indexes(db):
    db.books.create_index('id', unique=True)
    db.books.create_index('count')
    db.books.create_index('sum')
    db.books.create_index('author_ids')
    db.authors.create_index('id', unique=True)
    db.users.create_index('id', unique=True)
    db.ratings.create_index('user_id')
    db.ratings.create_index([("user_id", mongo.ASCENDING), ("book_id", mongo.ASCENDING)], unique=True)
    db.genres.create_index('id', unique=True)
    db.book_coords.create_index([("level", mongo.ASCENDING), ("lon", mongo.ASCENDING), ("lat", mongo.ASCENDING)], unique=True)
    db.book_coords.create_index([("lon", mongo.DESCENDING), ("lat", mongo.DESCENDING), ("sort", mongo.DESCENDING)])
    db.book_coords.create_index([('text_index', mongo.TEXT)], default_language='russian')

def load_ratings(db, filepath, batch_size=1000, log_period=10):
    inserted = 0
    total = 0
    last_logged = time.time()
    batch = []
    with open(filepath, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            (mark_id, user_id, work_id, rate) = row
            batch.append((int(user_id), int(work_id), float(rate)))
            total += 1
            if len(batch) > batch_size:
                bulk = db.ratings.initialize_unordered_bulk_op()
                for (user_id, work_id, rate) in batch:
                    inserted += 1
                    bulk.insert({
                        'user_id': user_id,
                        'book_id': work_id,
                        'rating': rate
                    })

                try:
                    r = bulk.execute()
                    total_inserted = r.get("nInserted")
                except mongo.errors.BulkWriteError as bwe:
                    total_inserted = bwe.details.get("nInserted")
                batch = []

            if (time.time() - last_logged) > log_period:
                logging.info("Processed %d rows, inserted %d rows" % (total, inserted))
                last_logged = time.time()

        if len(batch):
            inserted += 1
            bulk = db.ratings.initialize_unordered_bulk_op()
            for (user_id, work_id, rate) in batch:
                bulk.insert({
                    'user_id': user_id,
                    'book_id': work_id,
                    'rating': rate
                })

            try:
                r = bulk.execute()
                total_inserted = r.get("nInserted")
            except mongo.errors.BulkWriteError as bwe:
                total_inserted = bwe.details.get("nInserted")

        logging.info("Finished! Processed %d rows, inserted %d rows" % (total, inserted))

def load_authors(db, author_ids=[], check_db=True):
    logging.info("Start loading authors")
    total = 0
    batches = 0
    batch_size = 500
    last_logged = time.time()
    log_period = 10
    authors = []
    processed_authors = set([])
    pause_range = [0.5, 1.5]
    countries = {}

    if check_db:
        for b in db.authors.find():
            processed_authors.add(int(b['id']))

    if len(author_ids) == 0:
        author_ids = set([])
        for b in db.books.find():
            author_ids |= set(b['author_ids'])

    for author_id in list(author_ids):
        total += 1
        if author_id in processed_authors:
            continue

        url = 'https://fantlab.ru/autor%d.json' % int(author_id)
        if not cache.is_cached(url):
            pause = pause_range[0] + random.random() * (pause_range[1]-pause_range[0])
            time.sleep(pause)

        request = cache.cached(url, lambda: requests.get(url))
        if request.status_code != 200:
            print request.status_code, url
            if request.status_code == 503:
                cache.drop(url)
            continue
        try:
            author_data = json.loads(request.text)
        except Exception as e:
            print "not json: %s" % url
            continue


        country_id = int(author_data['country_id'])
        if country_id not in countries:
            country = CountryEntity()
            country.from_fantlab_dict(author_data)
            countries[country_id] = country
        country = countries[country_id]

        try:
            author = AuthorEntity()
            author.from_fantlab_dict(author_data)
            author.country = country

        except Exception as e:
            print e
            print url
            continue

        authors.append(author)
        processed_authors.add(author_id)

        if len(authors) > batch_size:
            bulk = db.authors.initialize_unordered_bulk_op()
            for author in authors:
                bulk.insert(author.to_dict())
            try:
                r = bulk.execute()
            except mongo.errors.BulkWriteError as bwe:
                total_inserted = bwe.details.get("nInserted")

            batches += 1
            authors = []
            last_logged = time.time()

        if (time.time() - last_logged) > log_period:
            logging.info("Processed %d authors, extracted %d authors, saved %d authors, countries %d" % (
                total, len(processed_authors), batches*batch_size, len(countries)))
            last_logged = time.time()

    if len(authors) > 0:
        bulk = db.authors.initialize_unordered_bulk_op()
        for author in authors:
            bulk.insert(author.to_dict())
        try:
            r = bulk.execute()
        except mongo.errors.BulkWriteError as bwe:
            total_inserted = bwe.details.get("nInserted")
        batches += 1

    # Missed authors, unavailable by API
    missed_authors = [
        {
            'id': 10,
            'name': 'Межавторский цикл',
            'country_id': 1,
            'name_original': '',
            'image_id': 10,
            'birthday': '2017-01-01',
            'deathday': None,
            'gender_male': True,
        },
        {
            'id': 100,
            'name': 'Межавторский цикл',
            'country_id': 1,
            'name_original': '',
            'image_id': 10,
            'birthday': '2017-01-01',
            'deathday': None,
            'gender_male': True,
        },
        {
            'id': 16974,
            'name': 'Сара Эддисон Аллен',
            'country_id': 2,
            'name_original': 'Sarah Addison Allen',
            'image_id': 16974,
            'birthday': '1971-00-00',
            'deathday': None,
            'gender_male': False,
        },
        {
            'id': 16975,
            'name': 'Борис Полевой',
            'country_id': 1,
            'name_original': '',
            'image_id': 16975,
            'birthday': '1908-03-04',
            'deathday': '1981-07-12',
            'gender_male': True,
        },
    ]
    for author in missed_authors:
        try:
            db.authors.insert(author)
        except Exception as e:
            continue


    logging.info("Finished! Processed %d authors, extracted %d authors, saved %d authors, countries %d" % (
                total, len(processed_authors), batches*batch_size, len(countries)))

    logging.info("Start loading countries")
    bulk = db.countries.initialize_unordered_bulk_op()
    for country_id in countries:
        bulk.insert(countries[country_id].to_dict())
    try:
        r = bulk.execute()
    except mongo.errors.BulkWriteError as bwe:
        total_inserted = bwe.details.get("nInserted")


def load_books(db, check_db=True):
    '''
    Суть такова - итерируемся по списку оценок в базе
        - обходим book_id оттуда
        - если еще не грузили, грузим
        - обходим авторов в данных
        - если автора еще не грузили, грузим
        - формируем книжку + засовываем внутрь авторов
        - если пачка
    :param db:
    :return:
    '''

    books = []
    pause_range=[0.5, 1.5]
    author_ids = set([])
    batch_size = 500
    last_logged = time.time()
    log_period = 10
    total = 0
    batches = 0

    processed_books = set([])
    total_books = 0
    for item in db.ratings.find():
        processed_books.add(item['book_id'])
    total_books = len(processed_books)

    processed_books = set([])
    if check_db:
        for b in db.books.find():
            processed_books.add(int(b['id']))

    logging.info("Start loading books")
    cursor = db.ratings.find(timeout=False)
    try:
        for item in cursor:
            total += 1
            book_id = int(item['book_id'])
            if book_id in processed_books:
                continue

            url = 'https://fantlab.ru/work%d.json' % book_id
            if not cache.is_cached(url):
                pause = pause_range[0] + random.random() * (pause_range[1]-pause_range[0])
                time.sleep(pause)


            request = cache.cached(url, lambda: requests.get(url))
            if request.status_code != 200:
                print request.status_code, url
                if request.status_code == 503:
                    cache.drop(url)
                continue

            try:
                data = json.loads(request.text)
                book = BookEntity()
                book.from_fantlab_dict(data)
            except Exception as e:
                print book_id
                print e
                continue

            books.append(book)
            processed_books.add(book_id)
            author_ids |= book.author_ids
            if len(books) > batch_size:
                bulk = db.books.initialize_unordered_bulk_op()
                for book in books:
                    bulk.insert(book.to_dict())
                try:
                    r = bulk.execute()
                except mongo.errors.BulkWriteError as bwe:
                    total_inserted = bwe.details.get("nInserted")

                batches += 1
                books = []
                last_logged = time.time()

            if (time.time() - last_logged) > log_period:
                logging.info("Processed %d rates, extracted %d books (%.1f%%), saved %d books, authors to load %d" % (
                    total, len(processed_books), len(processed_books)*100/float(total_books),  batches*batch_size, len(author_ids)))
                last_logged = time.time()
    except Exception as e:
        cursor.close()
        raise e


    if len(books) > 0:
        bulk = db.books.initialize_unordered_bulk_op()
        for book in books:
            bulk.insert(book.to_dict())
        try:
            r = bulk.execute()
        except mongo.errors.BulkWriteError as bwe:
            total_inserted = bwe.details.get("nInserted")

        batches += 1
        books = []
        last_logged = time.time()


    logging.info("Finished processed %d rates, extracted %d books, saved %d books, authors to load %d" % (
        total, len(processed_books), batches*batch_size, len(author_ids)))

def post_process_data(db):
    logging.info("Started")

    books_counts = {}
    author_counts = {}
    for item in db.ratings.find():
        book_id = int(item['book_id'])
        if book_id not in books_counts:
            books_counts[book_id] = 0
        books_counts[book_id] += 1
    logging.info("Loaded %d books counts" % len(books_counts))

    batch = []
    i = 0
    for book_id in books_counts:
        i += 1
        batch.append(book_id)
        if len(batch) > 1000 or i == len(books_counts):
            for item in db.books.find({'id': {'$in': batch}}):
                bid = int(item['id'])
                if item['author_ids']:
                    for author_id in item['author_ids']:
                        if author_id not in author_counts:
                            author_counts[author_id] = 0
                        author_counts[author_id] += books_counts[bid]
            batch = []
    logging.info("Loaded %d author counts" % len(author_counts))
    print len(batch)
    batch = []
    i = 0
    last_logged = time.time()
    for author_id in author_counts:
        i += 1
        author = db.authors.find_one({'id': author_id})
        if author:
            author['count'] = author_counts[author_id]
            db.authors.save(author)

        if (time.time() - last_logged) > 5:
            logging.info("Processed %d authors" % i)
            last_logged = time.time()

def export_to_csv(db, outdir):
    outdir = outdir.rstrip('/')
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    last_logged = time.time()
    log_period = 20

    for collection in ['authors', 'books', 'countries', 'ratings', 'book_coords', 'book_coords_meta']:
        logging.info("Processing collection %s" % collection)
    #for collection in ['authors', 'countries']:
        with open(outdir+'/'+collection+'.csv', 'w') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';')
            columns = db[collection].find().limit(1)[0].keys()
            columns = [str(c.encode('utf-8')) for c in columns if c != '_id']
            csvwriter.writerow(columns)
            total = db[collection].count()
            i = 0

            for row in db[collection].find():
                r = []
                for c in columns:
                    if c not in row or row[c] is None:
                        row[c] = 0

                    if type(row[c]) == list:
                        row[c] = ','.join([str(cc) for cc in row[c]])

                    v = row[c].encode('utf-8') if type(row[c]) not in {int, float, bool} else row[c] if c in row else ''
                    r.append(v)
                csvwriter.writerow(r)
                i += 1
                if (time.time() - last_logged) > log_period:
                    logging.info("  Processed %.2f%%" % (100.*i/total))
                    last_logged = time.time()
        logging.info("  Done")

def update_text_index(db):
    authors = {}
    books = {}
    book_ids = []
    i = 0
    last_logged = time.time()
    log_period = 20


    logging.info("Update title values")
    for item in db.book_coords.find():
        book_id = int(item['book_id'])
        book = db.books.find_one({'id': book_id})
        if book is None:
            logging.error("Book #%d not found" % book_id)
            continue

        title = book['title'] if book['title'] else book['title_original']
        db.book_coords.update({"_id": item['_id']}, {'$set': {'title': title}})
        i += 1
        if (time.time() - last_logged) > log_period:
            logging.info("Processed %d rows, collected %d books, %d authors" % (i, len(books), len(authors)))
            last_logged = time.time()


def import_from_csv(db, indir):
    indir = indir.rstrip('/')
    if not os.path.exists(outdir):
        logging.error("%s not exists" % indir)
        return False

    last_logged = time.time()
    log_period = 20
    batch_size = 400
    type_mappings = {
        'sort': float,
        'rating': float,
        'year_of_write': int,
        'is_foreign': bool,
        'lon': float,
        'image_id': int,
        'year': int,
        'gender_male': bool,
        'level': float,
        'count': int,
        'zoom_level': int,
        'user_id': int,
        'country_id': int,
        'id': int,
        'book_id': int,
        'lat': float,
    }
    for collection in ['authors', 'books', 'countries', 'ratings', 'book_coords', 'book_coords_meta']:

        logging.info("Processing collection %s" % collection)
    #for collection in ['authors', 'countries']:
        columns = []
        batch = []
        item_template = {}
        with open(indir+'/'+collection+'.csv', 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=';')
            db[collection].remove({})
            i = 0
            for row in csvreader:
                if i == 0:
                    columns = row
                    for c in columns:
                        item_template[c] = None
                    i += 1
                    continue

                item = item_template.copy()
                j = 0
                for c in columns:
                    v = row[j]

                    if c in type_mappings:
                        v = type_mappings[c](v)
                    elif c.find('_ids') != -1 or c in {'bounds', 'center'}:
                        v = eval('[' + v + ']')

                    item[c] = v
                    j += 1

                batch.append(item)

                if len(batch) > batch_size:
                    bulk = db[collection].initialize_unordered_bulk_op()
                    for item in batch:
                        bulk.insert(item)
                    try:
                        r = bulk.execute()
                    except mongo.errors.BulkWriteError as bwe:
                        total_inserted = bwe.details.get("nInserted")
                    batch = []
                i += 1
        if len(batch) > 0:
            bulk = db[collection].initialize_unordered_bulk_op()
            for item in batch:
                bulk.insert(item)
            try:
                r = bulk.execute()
            except mongo.errors.BulkWriteError as bwe:
                total_inserted = bwe.details.get("nInserted")

def convert(db):
    i = 0
    last_logged = time.time()
    log_period = 20

    logging.info("Update coord index to 2d")

    bounds_data = db.book_coords.aggregate({
        '$group': {
            '_id': "$item",
            'min_lon': { '$min': '$lon' },
            'max_lon': { '$max': '$lon' },
            'min_lat': { '$min': '$lat' },
            'max_lat': { '$max': '$lat' }
        }
    })
    bounds = bounds_data['result'][0]
    min_bounds = [float(bounds['min_lon']), float(bounds['min_lat'])]
    max_bounds = [float(bounds['max_lon']), float(bounds['max_lat'])]
    print 'bounds', min_bounds, max_bounds

    for item in db.book_coords.find():
        book_id = int(item['book_id'])
        book = db.books.find_one({'id': book_id})
        if book is None:
            logging.error("Book #%d not found" % book_id)
            continue

        coord = [item['lon'], item['lat']]
        db.book_coords.update({"_id": item['_id']}, {'$set': {'coord': coord}})
        i += 1
        if (time.time() - last_logged) > log_period:
            logging.info("Processed %d rows" % (i))
            last_logged = time.time()

    db.book_coords.drop_index('coord_2d_count_-1')
    db.book_coords.ensure_index([('coord', mongo.GEO2D), ('count', mongo.DESCENDING)],
                                min=[float(bounds['min_lon']), float(bounds['min_lat'])],
                                max=[float(bounds['max_lon']), float(bounds['max_lat'])])
    logging.info("2d-index created")

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)

parser = argparse.ArgumentParser()
parser.add_argument("--action", type=str, default='', help="What we should load: [ratings/books/authors]")
parser.add_argument("--ratings-file", type=str, default='', help="Path to file with marks")
parser.add_argument("--outdir", type=str, default='', help="Path to outdir")
parser.add_argument("--use-cache", type=str, default="1", help="1 to use cache")
parser.add_argument("--rewrite-cache", type=str, help="1 to renew cache")
parser.add_argument("--limit", type=str, help="selection limit (to debug)")
args = parser.parse_args()
args = vars(args)

actions = {'ratings', 'books', 'authors', 'postprocess', 'export', 'init', 'import', 'convert'}

action = args['action']
if action not in actions:
    parser.print_help()
    exit()

filepath = args['ratings_file']
if action == 'ratings' and not filepath:
    logging.error("Rating file not defined")
    parser.print_help()
    exit()

if action == 'ratings' and not os.path.exists(filepath):
    logging.error("Rating file not exists: %s" % filepath)
    exit()

outdir = args['outdir']

settings = load_settings('/etc/sky-fy.secret.json')
logging.info("Connecting to DB")
MONGO_CONN_STRING = "mongodb://%s:%s@%s:%s/books" % (settings['db']['user'],
                                               settings['db']['password'],
                                               settings['db']['host'],
                                               settings['db']['port'])

mng = mongo.MongoClient(MONGO_CONN_STRING)
db = mng.books

logging.info("Checking DB indexes")
create_indexes(db)

logging.info("Init cache")
cache = Cacher('fantlab_cache', '/home/akozlovtsev/tmp/cache/')

if action == 'init':
    logging.info("Database init")
    update_text_index(db)

if action == 'ratings':
    logging.info("Loading rates from %s" % filepath)
    load_ratings(db, filepath)

if action == 'convert':
    logging.info("Converting data")
    convert(db)

if action == 'books':
    #logging.info("Loading books from remote site")
    #load_books(db)
    logging.info("Loading authors from remote site")
    load_authors(db)

if action =='authors':
    logging.info("Loading authors from remote site")
    load_authors(db)

if action == 'postprocess':
    logging.info("Postprocessing data")
    post_process_data(db)

if action == 'export':
    logging.info("Export data to csv")
    export_to_csv(db, outdir)

if action == 'import':
    logging.info("Import data from csv")
    import_from_csv(db, outdir)

