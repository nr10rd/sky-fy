import hashlib
import os
import cPickle

class Cacher(object):
    def __init__(self, salt, cache_path, drop_mode=False, is_enabled=True):
        self.salt = salt
        self.cache_path = cache_path.rstrip('/') + '/'
        self.is_enabled = is_enabled
        self.drop_mode = drop_mode

    def _getkey(self, key):
        hsh = hashlib.md5()
        hsh.update(self.salt + '_' + key)
        return hsh.hexdigest()

    def is_cached(self, key):
        key = self._getkey(key)
        cache_path = self.cache_path+key+'.p'
        return os.path.exists(cache_path)

    def drop(self, key):
        key = self._getkey(key)
        cache_path = self.cache_path+key+'.p'
        return os.remove(cache_path)

    def cached(self, key, fn):
        key = self._getkey(key)
        cache_path = self.cache_path+key+'.p'
        result = None
        if not self.drop_mode:
            if os.path.exists(cache_path):
                result = cPickle.load(open(cache_path, 'rb'))

        if self.drop_mode or result is None:
            #print 'run '+key
            result = fn()
            cPickle.dump(result, open(cache_path, 'wb'))

        return result