class SimpleMapper:
    def __init__(self):
        self.nxt = 0
        self.ids = {}

    def ident(self, id):
        toid = self.ids.get(id, -1)
        if toid == -1:
            self.ids[id] = self.nxt
            toid = self.nxt
            self.nxt += 1
        return toid

    def hasId(self, id):
        r = self.ids.get(id)
        return True if r != -1 and r != None else False

    def len(self):
        return len(self.ids)

    def getById(self, id):
        return self.ids.get(id) if id != '' else None