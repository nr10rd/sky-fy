import os
import json
def load_settings(path):
    if not os.path.exists(path):
        raise Exception('Settings file %s not exists' % path)

    return json.load(open(path, 'r'))
