# -*- coding: utf-8 -*-

import cPickle
import logging
import argparse
import os

import pymongo as mongo
from rsvd import *
from MulticoreTSNE import MulticoreTSNE as TSNE
from utils.helpers import load_settings

def get_zoom_levels(cnts, max_zoom_levels=100, points_per_zoom=700):
    N = cnts.shape[0]
    zoom_ranges = []
    out = np.zeros((cnts.shape[0]), dtype=int)
    oldcnt = 1
    idx = (-cnts).argsort()

    ix = []
    for z in range(max_zoom_levels):
        zoom = z + 1
        cnt = int(min(points_per_zoom * 4**(z), N))
        print zoom, "[%d, %d]" % (oldcnt-1, cnt), " => [%d, %d]" % (cnts[idx[oldcnt-1]], cnts[idx[cnt-1]])
        zoom_ranges.append((cnts[idx[oldcnt-1]], cnts[idx[cnt-1]]))
        ix.append(idx[oldcnt-1:cnt-1])
        oldcnt = cnt
        if cnt == N:
            break
    return zoom_ranges, ix

def stretch_to_square(x, strength=0.4, inverse=True):
    bounds = [[x[:, 0].min(), x[:, 1].min()], [x[:, 0].max(), x[:, 1].max()]]
    W = bounds[1][0]-bounds[0][0]
    H = bounds[1][1]-bounds[0][1]
    R = (W + H)/4.
    R_h = H/2.
    R_w = W/2.
    sin45 = np.sin(np.pi / 4.)
    out = np.zeros(x.shape, dtype=np.float)
    for i in xrange(x.shape[0]):
        p = [np.abs(x[i, 0]), np.abs(x[i, 1])]
        p_norm = min(np.sqrt(p[0]**2 + p[1]**2), R)
        sin_a = p[1] / p_norm
        cos_a = p[0] / p_norm
        if sin_a >= sin45:
            R_hat = R*p_norm / p[1]
        else:
            R_hat = R*p_norm / p[0]

        df = (R_hat - R)*strength
        R_hat = R + df
        coeff = R_hat / R
        px = x[i, 0]
        py = x[i, 1]
        if inverse:
            px = (R-p_norm)*(x[i, 0] / p_norm)
            py = (R-p_norm)*(x[i, 1] / p_norm)
        px = px * coeff
        py = py * coeff
        out[i, 0] = px
        out[i, 1] = py
    return out

def add_noize(x, R_w=2., R_h=2.):
    out = np.zeros(x.shape, dtype=float)
    for i in xrange(x.shape[0]):
        phi = np.random.random()*2*np.pi
        r_h = np.random.normal(0., R_h/3.)
        r_w = np.random.normal(0., R_w/3.)
        out[i, 0] = x[i, 0] + np.cos(phi) * r_w
        out[i, 1] = x[i, 1] + np.sin(phi) * r_h
    return out

def get_book_title(book_id):
    index_string = []
    book = book_dict[book_id]
    authors = []
    is_foreign = True

    index_string.append(book['title'])
    if book['title_original']:
        index_string.append(book['title_original'])

    for author_id in book['author_ids']:
        if author_id not in author_dict:
            author_dict[author_id] = db.authors.find_one({'id': author_id})

        if author_id not in author_dict or author_dict[author_id] is None:
            print author_id, book_id, book['title']

        author = author_dict[author_id]

        if int(author['country_id']) == 1:
            is_foreign = False

        name = author['name_original'] if author['name_original'] else author['name']
        authors.append(name)
        if author['name']:
            index_string.append(author['name'])
        if author['name_original']:
            index_string.append(author['name_original'])

    author_name = ', '.join(authors)

    index_string = " | ".join(index_string).lower()

    title = book['title'] if book['title'] else book['title_original']
    return author_name, title, book['count'], book['year'], is_foreign, index_string

def get_color_index(item):
    y = '1' if (int(item['year']) > 1988) else '0'
    iy = '0' if y == '1' else '1'
    if int(item['book_id']) % 2:
        if bool(item['is_foreign']):
            c = '1%s0' % iy#(int(maxcolor), min(int(year*1.8), int(maxcolor)), int(year))
        else:
            c = '0%s1' % y  #(int(year), min(int(year*2.5), int(maxcolor)), int(maxcolor))
    else:
        if bool(item['is_foreign']):
            c = '1%s%s' % (y, y)#(int(maxcolor), min(int(year*1.8), int(maxcolor)), int(year))
        else:
            c = '%s%s1' % (y, y)  #(int(year), min(int(year*2.5), int(maxcolor)), int(maxcolor))
    return c

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)

parser = argparse.ArgumentParser()
parser.add_argument("--cache", type=int, default=1, help="")
parser.add_argument("--image", type=int, default=1, help="")
# parser.add_argument("--ratings-file", type=str, default='', help="Path to file with marks")
# parser.add_argument("--use-cache", type=str, default="1", help="1 to use cache")
# parser.add_argument("--rewrite-cache", type=str, help="1 to renew cache")
# parser.add_argument("--limit", type=str, help="selection limit (to debug)")
args = parser.parse_args()
args = vars(args)
is_use_cache = bool(args['cache'])
is_generate_image = bool(args['image'])

# =====================================================================================================================
logging.info("0. Loading dicts and mappers")

logging.info("Connecting to DB")
settings = load_settings('/etc/sky-fy.secret.json')
MONGO_CONN_STRING = "mongodb://%s:%s@%s:%s/books" % (settings['db']['user'],
                                               settings['db']['password'],
                                               settings['db']['host'],
                                               settings['db']['port'])

mng = mongo.MongoClient(MONGO_CONN_STRING)
db = mng.books


export_data = cPickle.load(open('books_export.p', 'rb'))
book_dict = export_data['books']
author_dict = export_data['authors']
book_mapper = export_data['book_mapper']
book_back_mapper = {book_mapper[x]:x for x in book_mapper}

# =====================================================================================================================
logging.info("1. Transformation")

if is_use_cache and os.path.exists('books_export_tsne.p'):
    books_tsne = cPickle.load(open('books_export_tsne.p', 'rb'))
else:
    books = export_data['vector']
    tsne = TSNE(n_jobs=6, perplexity=30)
    books_tsne = tsne.fit_transform(books)
    cPickle.dump(books_tsne, open('books_export_tsne.p', 'wb'))

# =====================================================================================================================
logging.info("2. Post-processing transformations")

max_zoom_levels = 100
points_per_zoom = 300
fit_to_square_strength = 0.
R_h = 2.
R_w = 1.
inverse_coords = False
metric = 'sum'
cnts = np.zeros((len(book_dict),), dtype=int)
for i in book_dict:
    if i in book_mapper:
        cnts[book_mapper[i]] = book_dict[i][metric]

zoom_ranges, ix = get_zoom_levels(cnts, max_zoom_levels=max_zoom_levels, points_per_zoom=points_per_zoom)
x = add_noize(stretch_to_square(books_tsne, strength=fit_to_square_strength, inverse=inverse_coords), R_h=R_h, R_w=R_w)

# =====================================================================================================================
logging.info("3. Save data")

db.book_coords.drop()
#db.book_coords.create_index([("level", mongo.ASCENDING), ("lon", mongo.ASCENDING), ("lat", mongo.ASCENDING)], unique=True)
db.book_coords.create_index([("lon", mongo.DESCENDING), ("lat", mongo.DESCENDING), ("sort", mongo.DESCENDING)])
db.book_coords.create_index([('text_index', mongo.TEXT)], default_language='russian')
db.book_coords_meta.remove({})
batch = []
batch_size = 500
authors = {}
books = {}
i = 0
for book_id in book_mapper:
    bid = book_mapper[book_id]
    book_metric = book_dict[book_id][metric]
    level = 0
    for cnt_range in zoom_ranges:
        if book_metric >= cnt_range[1] and book_metric < cnt_range[0]:
            break
        level += 1

    author_title, book_title, count, year, is_foreign, index_string = get_book_title(book_id)

    item = {
        'book_id': int(book_id),
        'title': book_title,
        'author': author_title,
        'year': year,
        'count': count,
        'is_foreign': is_foreign,
        'level': int(level),
        'lon': float(x[bid, 0]),# x
        'lat': float(x[bid, 1]),# y,
        'text_index': index_string,
        'sort': np.log(max(count, 1)),
        'color': '111'
    }

    item['color'] = get_color_index(item)

    batch.append(item)

    i += 1
    if batch and (len(batch) > batch_size or i == len(book_mapper)):
        bulk = db.book_coords.initialize_unordered_bulk_op()
        for item in batch:
            bulk.insert(item)
        try:
            r = bulk.execute()
            total_inserted = r.get("nInserted")
        except mongo.errors.BulkWriteError as bwe:
            total_inserted = bwe.details.get("nInserted")
        batch = []

db.book_coords_meta.insert({
    'bounds': [[x[:, 0].min(), x[:, 1].min()], [x[:, 0].max(), x[:, 1].max()]],
    'center': [[x[:, 0].mean(), x[:, 1].mean()]],
    'zoom_level': len(zoom_ranges)
})
logging.info("Finished!")