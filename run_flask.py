from flask import Flask, render_template, request
import json
import pymongo as mongo
import logging
import requests
import timeit
from bin.utils.helpers import *

def load_config():
    pass

app = Flask(__name__)

settings = load_settings('/etc/sky-fy.secret.json')

logging.info("Connecting to DB")
MONGO_CONN_STRING = "mongodb://%s:%s@%s:%s/books" % (settings['db']['user'],
                                               settings['db']['password'],
                                               settings['db']['host'],
                                               settings['db']['port'])
if mongo.version == '2.7.2':
    mng = mongo.MongoClient(MONGO_CONN_STRING, _connect=False)
else:
    mng = mongo.MongoClient(MONGO_CONN_STRING, connect=False)

db = mng.books
limit = 1000
@app.route('/')
def index():
    return map()

@app.route('/map/')
def map():
    lat = request.args.get('lat', 0.)
    lng = request.args.get('lng', 0.)
    zoom = int(request.args.get('zoom', 10))
    limit = min(int(request.args.get('limit', 1000)), 3000)
    titles_mode = request.args.get('titles', '')
    if titles_mode not in {'no', 'top', 'many'}:
        titles_mode = 'top'
    map_meta = db.book_coords_meta.find_one()
    params = {
        'map': {
            'limit': limit,
            'titles': titles_mode,
            'bounds': map_meta['bounds'],
            'center': [lng, lat],#map_meta['center'],
            'zoom': zoom,
            'zoom_levels': map_meta['zoom_level']
        }
    }
    return render_template('map.min.html', params=params)

@app.route('/api/suggests/', methods=['GET'])
def get_suggests():
    q = request.args.get('q', default='', type=unicode)
    out = []
    limit = 20
    if q and len(q) > 2:
        res = db.command("text", "book_coords", search=q.lower(), limit=limit)
        if 'results' in res:
            for item in res['results']:
                out.append({
                    'id': item['obj']['book_id'],
                    'name': item['obj']['author'] + ' - ' + item['obj']['title'],
                    'point': [item['obj']['lat'], item['obj']['lon']]
                })


    return json.dumps({
        'q': q,
        'items': out
    }), 200, {'Content-Type': 'application/json; charset=utf-8'}

@app.route('/api/info/', methods=['GET'])
def get_info():
    args = dict(request.args)
    book_id = int(args.get('book_id', 0)[0])
    book_data = db.books.find_one({'id': book_id})
    if book_data:
        book_data = dict(book_data)
        del book_data['_id']
        book_data['image_url'] = 'https://data.fantlab.ru/images/editions/small/%d' % book_id
        book_data['book_url'] = 'https://fantlab.ru/work%d' % book_id
        book_data['authors'] = []
        book_data['title'] = book_data['title'] if book_data['title'] else book_data['title_original']
        for author in db.authors.find({'id': {'$in':book_data['author_ids']}}):
            book_data['authors'].append({
                'name': author['name'],
                'url': 'https://fantlab.ru/autor%d' % author['id']
            })
        if 'annotation' not in book_data or book_data['annotation'] == '':
            url = "https://fantlab.ru/work%d.json" % book_id
            httprequest = requests.get(url)
            if httprequest.status_code == 200 and httprequest.text != '':
                work_data = json.loads(httprequest.text)
                if 'work_description' in work_data and work_data['work_description'] != '':
                    book_data['annotation'] = work_data['work_description']
                    db.books.update({'id': book_id}, {'$set': {'annotation': book_data['annotation']}})
    else:
        book_data = {}

    return json.dumps(book_data), 200, {'Content-Type': 'application/json; charset=utf-8'}


@app.route('/api/coords/', methods=['GET'])
def get_coords():
    bounds = []
    args = dict(request.args)
    bounds.append(args.get('bounds_sw[]', []))
    bounds.append(args.get('bounds_ne[]', []))
    zoom = int(args.get('zoom', 0)[0])
    limit = min(int(args.get('limit', 0)[0]), 5000)
    titles_mode = request.args.get('titles', '')
    tail_size = int(limit * .2)
    points = []

    #title_cutoff = int(limit * 0.08) if titles_mode != 'no' else 0
    title_cutoff = int(200) if titles_mode != 'no' else 0
    i = 0

    #db.book_coords.find({ 'coord': { $geoWithin: { $box: [[-5., -5.], [5., 5.]] }} }).sort({ 'count' : 1  }).limit(10)
    starttime = timeit.default_timer()
    elapsed = None
    for item in db.book_coords.find({
            'lon': {
                '$lte': float(bounds[1][0]),
                '$gte': float(bounds[0][0])
            },
            'lat': {
                '$lte': float(bounds[1][1]),
                '$gte': float(bounds[0][1])
            }
        }).sort([('sort', mongo.DESCENDING)]).limit(limit):
    # for item in db.book_coords.find({
    #     'coord': {
    #         '$geoWithin': {
    #             '$box': [
    #                 [float(bounds[0][0]), float(bounds[0][1])],
    #                 [float(bounds[1][0]), float(bounds[1][1])]
    #             ]
    #         }
    #     }
    # }).sort([('sort', mongo.DESCENDING)]).limit(limit):
        if elapsed is None:
            elapsed = timeit.default_timer() - starttime
        #min_y=1960., max_y=2017.
        # y = '1' if (int(item['year']) > 1988) else '0'
        # iy = '0' if y == '1' else '1'
        # if int(item['book_id']) % 2:
        #     if bool(item['is_foreign']):
        #         c = '1%s0' % iy#(int(maxcolor), min(int(year*1.8), int(maxcolor)), int(year))
        #     else:
        #         c = '0%s1' % y  #(int(year), min(int(year*2.5), int(maxcolor)), int(maxcolor))
        # else:
        #     if bool(item['is_foreign']):
        #         c = '1%s%s' % (y, y)#(int(maxcolor), min(int(year*1.8), int(maxcolor)), int(year))
        #     else:
        #         c = '%s%s1' % (y, y)  #(int(year), min(int(year*2.5), int(maxcolor)), int(maxcolor))

        # points.append({
        #     'b': item['book_id'],
        #     't': item['title'] if i < title_cutoff else '',
        #     'a': item['author'] if i < title_cutoff else '',
        #     #'year': item['year'],#
        #     'cnt': item['count'],
        #     #'is_foreign': bool(item['is_foreign']),#
        #     #'level': item['level'],#
        #     'ln': item['lon'],# x
        #     'lt': item['lat'],# y,
        #     'c': item['color']
        # })
        points.append([
            item['book_id'],#0
            item['title'] if i < title_cutoff else '',#1
            item['author'] if i < title_cutoff else '',#2
            item['count'],#3
            item['lon'],# x 4
            item['lat'],# y, 5
            item['color'] #6
        ])
        i += 1


    # uid = int(request.args.get('uid', type=int, default=0))
    # mid = int(request.args.get('mid', type=int, default=0))

    return json.dumps({
        'limit': limit,
        #'elapsed': elapsed,
        'zoom': zoom,
        'points': points
    }), 200, {'Content-Type': 'application/json; charset=utf-8'}