#!/bin/bash

SCRIPT_DIR=$(dirname $(readlink -e $BASH_SOURCE))

cp -rf $SCRIPT_DIR/* /home/sites/skyfi/
rm -rf /home/sites/skyfi/config
rm -rf /home/sites/skyfi/*.sh
