function request(url, data, success, error, type) {
    type = type ? type: 'json';
    return $.ajax({
      url: url,
      type: 'GET',
      dataType: type,
      data: data,
      success: function(response) {
            success(response);
      },
      error: function(response) {
         if (error)
            error(response);
         else
            alert('Server not responds')
      }
    });
}

function generate_icon_templates(sizes) {
    var out = []
    for (var i = 0; i < sizes.length; i++) {
        var size = sizes[i];
        out.push({
            'src': 'star_'+size+'_',
            'class': 'map-icon-'+size,
            'size': [[size, size], [(size/2)|0, (size/2)|0]]
        })
    }
    return out
}

function generate_colormap(cols) {
    var out = {}
    for (var i = 0; i < cols.length; i++) {
        out[cols[i]] = ['#', '#']
        for (var c = 0; c < cols[i].length; c++) {
            out[cols[i]][0] +=  cols[i][c] == '1' ? 'FF' : 'FF';
            out[cols[i]][1] +=  cols[i][c] == '1' ? '88' : '11';
        }
    }
    return out;
}
var colormap = generate_colormap(['000','100','010','001','110','011','101','111']);

var xy = function(x, y) {
    if (L.Util.isArray(x)) {    // When doing xy([x, y]);
        return yx(x[1], x[0]);
    }
    return yx(y, x);  // When doing xy(x, y);
};
var yx = L.latLng;

var mobileAndTabletcheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

var map_wrapper = {
    zoomOffset: 5,
    lastLayer: null,
    points_offset: [0., 0.],
    zoom_range: [5, 17, 24],
    size: [22145, 22145],
    points_count: 2000,
    star_sizes: [64,56,48,40,32,24,16],

    points_bounds: [],
    points_ranges: [],
    map_bounds: [],
    map_ranges: [],
    map_offset: [],
    points_to_map_scale: [],
    map_to_points_scale: [],
    map_size: [],
    params: {},
    map: null,
    last_map_request: null,
    request_timer: null,
    suggest_cache: {},
    renderer: L.canvas(),
    popup_marker: null,
    marker_state: null,
    popup_timer: 0,
    deffered_popup_id: null,
    deffered_marker: null,
    push_state: function(params) {
        var query = $.param(params);
        history.pushState(params, query, '/?'+query)
    },
    pop_state: function(params, is_refresh) {
        var center = [
            this.scale_to(params['lat'], this.points_ranges[0], this.map_ranges[0]),
            this.scale_to(params['lng'], this.points_ranges[1], this.map_ranges[1])
        ];
        this.map.setView(xy(center), params['zoom']);
        if (is_refresh) {
            this.refresh();
        }
    },
    scale_to: function(v, src, trg) {
        return trg[0] + ((v-src[0])/(src[1]-src[0]))*(trg[1] - trg[0]);
    },
    show_popup: function(marker) {
        this.popup_timer = + new Date();
        if (this.popup_marker) {
            this.hide_popup();
        }
        this.popup_marker = marker;
        this.marker_state = marker.getRadius();
        marker.setRadius(10);
        var xy = marker._point;
        var screen_bounds = this.map._container.getBoundingClientRect(),
            point = marker._latlng,
            map_bounds = this.map.getBounds();

        var xy = {
            x: this.scale_to(point.lng, [map_bounds._southWest.lng, map_bounds._northEast.lng], [screen_bounds.left, screen_bounds.right]),
            y: this.scale_to(point.lat, [map_bounds._northEast.lat, map_bounds._southWest.lat], [screen_bounds.top, screen_bounds.bottom]),
        };
        var el = $('#popup');
        var data = marker.options.data;
        el.css({
            'position': 'absolute',
            'display': 'block',
            'top': xy.y,
            'left': xy.x
        });
        el.html('<h4 class="popup-authors">'+data['author']+'</h4><h3 class="popup-title">'+data['title']+'</h3><p class="popup-description"><img src="/static/ajax-loader.gif"></p>')
        el.show();
        request('/api/info/?book_id='+data['book_id'], {}, function(response) {
            var anno = response['annotation'] ? response['annotation'] : '';
            var authors = []
            for (var i = 0; i< response['authors'].length; i++) {
                authors.push('<a target="_blank" href="'+response['authors'][i]['url']+'">'+response['authors'][i]['name']+'</a>');
            }
            authors = authors.join(', ');

            var booktype = (response['type'] ? response['type'] : '') + (response['year'] ? ' ('+response['year']+')' : '');
            var title = '<a target="_blank" href="'+response['book_url']+'">'+response['title']+'</a>'
            $('#popup .popup-title').html(title);
            $('#popup .popup-authors').html(authors);
            $('#popup .popup-description').html('<b>'+booktype+'</b><br><img class="popup-img" src="'+response['image_url']+'"><span class="popup-description">'+anno+'</span>');
        });
    },
    hide_popup: function() {
        if (this.popup_marker) {
            this.popup_marker.setRadius(this.marker_state);
            this.popup_marker = null;
        }
        $('#popup').hide()
    },
    create_layer_vector: function(points, zoom) {
        var d = (map_wrapper.params['limit']*1/this.star_sizes.length) | 0;
        var markers = [];
        for (var i = points.length-1; i >=0; i--) {
            var j = Math.min(Math.round(i / d), this.star_sizes.length-1);
            var title = points[i][1] ? points[i][2] + ' - ' + points[i][1] : '';
            var coords = [
                (this.scale_to(points[i][4], this.points_ranges[0], this.map_ranges[0])),
                this.scale_to(points[i][5], this.points_ranges[1], this.map_ranges[1])
            ];

            var radius = (this.star_sizes[j]/3.) * Math.max(zoom-19, 1);
            var opac = this.scale_to(Math.log(points[i][3]), [0, 9.6], [0.2, .7]);
            var z = this.scale_to(zoom, [5, 24], [0.2, 0.7]);
            var topcolor = colormap[points[i][6]][0];
            var botcolor = colormap[points[i][6]][1];
            var marker = L.circleMarker(xy(coords), {
                renderer: this.renderer,
                color: botcolor,
                fillColor: topcolor,
                weight: radius*0.85,
                radius: radius*0.15,
                opacity: 0.1,
                fillOpacity: opac,//1.,
                data: {
                    book_id: points[i][0],
                    title: points[i][1],
                    author: points[i][2],
                }

            });
            if (this.params['titles'] != 'no' && title) {
                marker.bindTooltip(title, {
                    opacity: z,
                    permanent: true,//(this.params['titles'] == 'many' && j <= 2) || (this.params['titles'] == 'top' && j == 0)  || zoom >= (this.zoom_range[2]-5),
                    direction: 'top'
                });
            }

            var that = this;
            marker.on('click', function(e, m){
                that.show_popup(this);
            });
            markers.push(marker)
            if (this.deffered_popup_id == points[i][0]) {
                this.deffered_marker = marker;
            }
        }
        return L.layerGroup(markers)
    },
    disable_map: function() {
        this.map.dragging.disable();
        this.map.touchZoom.disable();
        this.map.doubleClickZoom.disable();
        this.map.scrollWheelZoom.disable();
        this.map.boxZoom.disable();
        this.map.keyboard.disable();
        if (this.map.tap) this.map.tap.disable();
    },
    enable_map: function() {
        this.map.dragging.enable();
        this.map.touchZoom.enable();
        this.map.doubleClickZoom.enable();
        this.map.scrollWheelZoom.enable();
        this.map.boxZoom.enable();
        this.map.keyboard.enable();
        if (this.map.tap) this.map.tap.enable();
    },
    refresh: function(keep_state) {

        var b = this.map.getBounds();
        var view_bounds = [
            [ this.scale_to(b['_southWest']['lng'], this.map_ranges[0], this.points_ranges[0]),
              this.scale_to(b['_southWest']['lat'], this.map_ranges[1], this.points_ranges[1]) ],
            [ this.scale_to(b['_northEast']['lng'], this.map_ranges[0], this.points_ranges[0]),
              this.scale_to(b['_northEast']['lat'], this.map_ranges[1], this.points_ranges[1]) ],
        ];
        var that = this;
        if (this.last_map_request) {
            this.last_map_request.abort();
        }
        this.last_map_request = request('/api/coords/', {
            'limit': this.params['limit'],
            'bounds_sw': view_bounds[0],
            'bounds_ne': view_bounds[1],
            'zoom': this.map.getZoom(),
            'titles': this.params['titles']
        }, function(response){
            that.disable_map();
            if (that.lastLayer) {
                that.lastLayer.clearLayers();
                that.map.removeLayer(that.lastLayer);
                that.lastLayer.remove();
            }
            if (keep_state) {
                that.push_state({
                    lat: (view_bounds[0][1] + view_bounds[1][1]) / 2,
                    lng: (view_bounds[0][0] + view_bounds[1][0]) / 2,
                    zoom: that.map.getZoom(),
                    limit: that.params['limit'],
                    titles: that.params['titles']
                });
            }
            that.lastLayer = that.create_layer_vector(response.points, that.map.getZoom());
            that.map.addLayer(that.lastLayer);
            if (that.deffered_marker) {
                that.show_popup(that.deffered_marker)
            }
            that.deffered_marker = null;
            that.deffered_popup_id = null;
            that.enable_map();
        }, function(response) {});
    },
    toggleButton: function(el) {
        el.parent().find('button').removeClass('active');
        el.addClass('active');
        this.params[el.attr('data-field')] = el.attr('data-value');
    },
    init: function(params) {
        this.params = params;
        var points_bounds = this.params['bounds'];

        var phase_x = .5,
            phase_y = -0.177,
            multi_x = 0.9961,
            multi_y = 0.9806;

        var points_offset = [
            [-2.652597, -2.022196],
             [2.762384, 4.303819]
        ];
        var image_bounds =  [
            [-749, -608], [22780, 23294]
        ];
        var points_bounds_fitted = [
            [multi_x * (points_bounds[0][0]+points_offset[0][0]) + phase_x, multi_y * (points_bounds[0][1]+points_offset[0][1]) + phase_y],
            [multi_x * (points_bounds[1][0]+points_offset[1][0]) + phase_x, multi_y * (points_bounds[1][1]+points_offset[1][1]) + phase_y]
        ];
        var points_size = [ points_bounds[1][0] - points_bounds[0][0], points_bounds[1][1] - points_bounds[0][1] ];
        L.CRS.CustomZoom = L.extend({}, L.CRS.Simple, {
            scale: function (zoom) {
                return Math.pow(1.3, zoom);
            }
        });

        this.map = L.map('map', {
            renderer: this.renderer,
            crs: L.CRS.CustomZoom,
            minZoom: this.zoom_range[0],
            maxZoom: 22,
            zoomControl: true,
            bounceAtZoomLimits: false,
            touchZoom: true,
            //scrollWheelZoom: false,
            //wheelDebounceTime: 200,
            wheelPxPerZoomLevel: 220
        });

        if (mobileAndTabletcheck()) {
            this.points_count = 1000;
            this.toggleButton($('.menu-selector button:contains("500")'))
        }

        L.control.zoom({
             position:'bottomleft'
        }).addTo(this.map);

        var that = this;
        var mapBounds = new L.LatLngBounds(
            this.map.unproject([0, this.size[0]], this.zoom_range[1]),
            this.map.unproject([this.size[1], 0], this.zoom_range[1])
        );

        var map_size = [ mapBounds['_northEast']['lng'] - mapBounds['_southWest']['lng'], mapBounds['_northEast']['lat'] - mapBounds['_southWest']['lat'] ];
        this.map_size = map_size;
        this.points_to_map_scale = [ map_size[0] / points_size[0], map_size[1] / points_size[1] ];
        this.map_to_points_scale = [ 1 / this.points_to_map_scale[0], 1 / this.points_to_map_scale[1] ];
        this.map_bounds = [ mapBounds['_southWest']['lng'], mapBounds['_southWest']['lat'], mapBounds['_northEast']['lng'], mapBounds['_northEast']['lat'] ];
        this.map_ranges = [ [mapBounds['_northEast']['lng'], mapBounds['_southWest']['lng']],
                             [mapBounds['_northEast']['lat'], mapBounds['_southWest']['lat']]];
        this.points_bounds = [ points_bounds[0][0], points_bounds[0][1], points_bounds[1][0], points_bounds[1][1] ];
        this.points_ranges = [ [points_bounds_fitted[1][0], points_bounds_fitted[0][0]],
                               [points_bounds_fitted[1][1], points_bounds_fitted[0][1]] ];
        this.map_offset = [
            this.scale_to(Math.abs(points_bounds_fitted[0][0]), this.points_ranges[0], this.map_ranges[0])-this.scale_to(Math.abs(points_bounds[0][0]), this.points_ranges[0], this.map_ranges[0]),
            this.scale_to(Math.abs(points_bounds_fitted[0][1]), this.points_ranges[1], this.map_ranges[1])-this.scale_to(Math.abs(points_bounds[0][1]), this.points_ranges[1], this.map_ranges[1]),
        ];
        this.map.fitBounds(mapBounds);
        var tile_url = '/static/tiles/{z}/{x}/{y}.png';
        var layer = L.tileLayer(tile_url, {
              minZoom: this.zoom_range[0],
              maxZoom: 19,
              bounds: mapBounds,
              attribution: '',
              noWrap: true,
              tms: false
        }).addTo(this.map);

        this.pop_state({
            'lat': this.params['center'][0],
            'lng': this.params['center'][1],
            'zoom': this.params['zoom']
        }, true);

        this.map.on('click', function(e){
            var time = + new Date();
            if (that.popup_timer > 0 && (time - that.popup_timer)>200) {
                that.hide_popup();
            }

        });
        this.map.on('movestart', function(){
            that.hide_popup();
        });

        this.map.on('move', function(){
            if (that.request_timer) {
                clearTimeout(that.request_timer);
                that.request_timer = null;
            }

            that.request_timer = setTimeout(function(){
                that.hide_popup();
                that.refresh(true);
            }, 500);
        });

        $(window).on('popstate', function(e) {
            that.pop_state(e.originalEvent.state)
        });

        $('.typeahead').typeahead({
            name: 'test',
            minLength: 3,
            items: 20,
            display: 'name',
            source: function(q, process) {
                return $.get('/api/suggests/', {'q': q},
                    function (response) {
                        var data = new Array();
                        $.each(response.items, function(i, item){
                            that.suggest_cache[item['id']] = item;
                            data.push('#'+item['id'] + ' ' + item['name']);
                        });
                        return process(data);
                    },
                'json'
                );
            },
            matcher: function(q) {
                return true;
            },
            updater: function(str) {
                var book_id = str.split(" ")[0].substring(1) - 0;
                var item = that.suggest_cache[book_id];
                that.deffered_popup_id = item['id']
                that.pop_state({
                    'lat': item['point'][1]+0.2,
                    'lng': item['point'][0]-0.2,
                    'zoom': 19
                });
            }
        });

        $('.menu-selector button').on('click', function() {
            that.toggleButton($(this));
            that.refresh(true);
        })
    }
}

